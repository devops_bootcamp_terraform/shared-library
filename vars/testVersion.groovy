#!/usr/bin/env groovy

def call() {
    def version
    def registry_url
    if (BRANCH_NAME == null) {
        error "BRANCH_NAME environment variable not set"
    }
    else if (BRANCH_NAME.startsWith("feature/") || BRANCH_NAME == "develop" || BRANCH_NAME == "preprod") {
        version = "${BRANCH_NAME}-SNAPSHOT-${BUILD_NUMBER}"
        registry_url = "3.82.155.119:8083/dev"
    } else if (BRANCH_NAME == "main") {
        version = "1.0.${BUILD_NUMBER}"
        registry_url = "3.82.155.119:8083/prod"
    } else {
        error "Unsupported branch: ${BRANCH_NAME}"
    }
}

