#!/usr/bin/env groovy
package com.example

class Docker implements Serializable {
    def script
    Docker(script) {
        this.script = script
    }
    def builddockerImage(String imageName) {
        script.sh "docker build -t ${imageName} ."
    }
    def logindockerRepo() {
        script.withCredentials([
        script.usernamePassword(credentialsId: 'nexus-cred', usernameVariable: 'USER', passwordVariable: 'PWD')
    ]) {
            script.sh "echo $script.PWD | docker login -u $script.USER --password-stdin ${script.env.REGISTRY}"
           
        }  
    }
    def pushdockerImage(String imageName) {
        script.sh "docker push ${imageName}"
    }
}
