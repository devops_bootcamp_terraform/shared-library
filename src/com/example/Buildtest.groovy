#!/usr/bin/env groovy
package com.example

class Buildtest implements Serializable {
    def script
    Buildtest(script) {
        this.script = script
    }
    def unitTest() {
        script.sh 'mvn test'
    }
    def buildJar() {
        script.sh 'mvn clean package'
        script.echo "the build stage for ${script.BRANCH_NAME}"
    }
}