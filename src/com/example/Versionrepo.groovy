#!/usr/bin/env groovy
package com.example

class Versionrepo implements Serializable {
    def script
    Versionrepo(script) {
        this.script = script
    }
    def repoAdd() {
        if (script.BRANCH_NAME == null) {
        error "BRANCH_NAME environment variable not set"
        }
        else if (script.BRANCH_NAME.startsWith("feature/") || script.BRANCH_NAME == "develop" || script.BRANCH_NAME == "preprod") {
            return "3.82.155.119:8083/dev"
        } else if (script.BRANCH_NAME == "main") {
            return "3.82.155.119:8083/prod"
        } else {
            error "Unsupported branch: ${script.BRANCH_NAME}"
        }
    }
    def versionAdd() {
        if (script.BRANCH_NAME == null) {
        error "${script.env.BRANCH_NAME} environment variable not set"
        }
        else if (script.BRANCH_NAME.startsWith("feature/") || script.BRANCH_NAME == "develop" || script.BRANCH_NAME == "preprod") {
            return "${script.BRANCH_NAME}-SNAPSHOT-${script.BUILD_NUMBER}"
        } else if (BRANCH_NAME == "main") {
            return "1.0.${script.BUILD_NUMBER}"
        } else {
            error "Unsupported branch: ${script.env.BRANCH_NAME}"
        }
    }
}